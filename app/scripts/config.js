/**
 * LUNA - Responsive Admin Theme
 * version 1.2
 *
 */

function configState($stateProvider, $urlRouterProvider, $compileProvider) {

    // Optimize load start with remove binding information inside the DOM element
    $compileProvider.debugInfoEnabled(true);

    // Set default state
    $urlRouterProvider.otherwise("/main/main");
    $stateProvider

        // Main content
        .state('main', {
            abstract: true,
            url: "/main",
            templateUrl: "views/common/content.html"

        })
        .state('main.main', {
            url: "/main",
            templateUrl: "views/main.html",
            data: {
                pageTitle: 'Main'
            }
        })
}

angular
    .module('luna')
    .config(configState)
    .run(function($rootScope, $state, $http) {
        $rootScope.$state = $state;
        $rootScope.currentTodo = 0;
        // PURPOSEFULLY not putting the data stuff into a factory,
        // the assignment is simple so not worth doing for one call
        $http({
            method: 'GET',
            url: 'https://schoolinks-carlos-backend.herokuapp.com/todolists/'
        }).then(function successCallback(response) {
            $rootScope.model = response.data
        }, function errorCallback(response) {
            $rootScope.model = [
                {
                    name : "Primary" , list : [
                    { taskName : "Write a todo list app in angular" , isDone : false },
                    { taskName : "Re-use an old template to start this app" , isDone : true },
                    { taskName : "Create a backend to power this todo app" , isDone : true },
                    { taskName : "Double check to make sure I dont forget a feature" , isDone : true }
                ]
                },
                {
                    name : "Secondary" , list : [
                    { taskName : "Drink coffee" , isDone : false },
                    { taskName : "Take a Nap" , isDone : false },
                    { taskName : "Clean apartment" , isDone : false },
                    { taskName : "Buy new Macbook" , isDone : false },
                    { taskName : "Get Dinner" , isDone : false }
                ]
                }
            ];
        });

    });