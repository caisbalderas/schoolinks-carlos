/**
 *
 * todoCtrl
 *
 */

angular
    .module('luna')
    .controller('todoCtrl', todoCtrl);

function todoCtrl($scope, $rootScope){

    $scope.show = "All";  // Want everything to show at first
    $scope.todos = $scope.model[0].list;  // Show first list as the default
    $scope.model = $rootScope.model;  // Keep it in the root scope for now.. could make move to factory

    $scope.completedCount = function() {
        var count = 0;
        angular.forEach($scope.todos, function(todo){
            count += todo.isDone ? 1 : 0;
        });
        return count;
    };

    $scope.addTodo = function  () {
        $scope.todos.splice(0,0,{taskName : $scope.newTodo , isDone : false });
        $scope.newTodo = "";  // reset this value
    };

    $scope.addTodoList = function (index) {
        var newList = {
            name : $scope.newTodoList ,
            list : []
        };
        $scope.model.splice(0, 0, newList);
        $scope.newTodoList = "";  // reset this value
    };

    $scope.deleteTodo = function (index) {
        $scope.todos.splice(index, 1);
    };

    $scope.deleteTodoList = function (index) {
        $scope.model.splice(index, 1);
    };

    $scope.selectTodo = function (index) {
        $scope.currentTodo = index;
        $scope.todos = $scope.model[$scope.currentTodo].list;
    };

    /* Filter Function for All | Incomplete | Complete */
    $scope.showFn = function  (todo) {
        if ($scope.show === "All") {
            return true;
        }else if(todo.isDone && $scope.show === "Complete"){
            return true;
        }else if(!todo.isDone && $scope.show === "Incomplete"){
            return true;
        }else{
            return false;
        }
    }


}
